import Vue from 'vue'
import App from './App.vue'
import Vuex from 'vuex';
import Router from 'vue-router'
import Vuesax from 'vuesax';

Vue.config.productionTip = false

import store from './store/store'

//css -Imports needed for vuesax
import 'vuesax/dist/vuesax.css';
import 'material-icons/iconfont/material-icons.css';

Vue.use(Vuex)
Vue.use(Vuesax)
Vue.use(Router)

new Vue({
  render: h => h(App),
  store: store
}).$mount('#app')
