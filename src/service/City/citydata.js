import { cityinstance } from '../axios'

export const getAllCityData = async () => {
    return await cityinstance.get('/records/1.0/search/?dataset=1000-largest-us-cities-by-population-with-geographic-coordinates&sort=-rank&facet=city&facet=state')
}

export const getCityData=async(city)=>{
    return await cityinstance.get();
}