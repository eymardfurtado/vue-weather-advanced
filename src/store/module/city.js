import * as cityapi from '../../service/City/citydata'

const state = {
    cities: []
}

const getters = {
    getCities: (state) => {
        return state.cities
    }
}

const mutations = {
    updateCities: (state, payload) => {
        state.cities = payload['data']['records'];
    }
}

const actions = {
    loadCities: ({ commit }) => {
        let citydata = cityapi.getAllCityData();
        citydata.then((data) => {
            if (data && data['status']) {
                commit('updateCities', data);
            }
        })
    }
}

export default {
    state,
    getters,
    mutations,
    actions
}